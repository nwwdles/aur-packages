all: init rebuild

init:
	git submodule init
	git submodule update
	git submodule foreach git config --local core.excludesfile "../.gitignore_submodules"

rebuild:
	for dir in ./*/; do \
		if [ -e "$dir" ]; then (cd "$dir" && makepkg -fc && makepkg --printsrcinfo >.SRCINFO); fi \
	done

.PHONY: all init rebuild
