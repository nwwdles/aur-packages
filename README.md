# aur-packages

This repo is just for my convenience.

## resources

- https://wiki.archlinux.org/index.php/VCS_package_guidelines
- https://wiki.archlinux.org/index.php/Arch_User_Repository#Submitting_packages
- https://wiki.archlinux.org/index.php/AUR_submission_guidelines#Authentication
- https://help.github.com/en/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent
- [PKGBUILD for git repo example](https://git.archlinux.org/pacman.git/tree/proto/PKGBUILD-vcs.proto#n33)

## my repos list

- [plasma5-applets-virtual-desktop-bar-git](https://aur.archlinux.org/packages/plasma5-applets-virtual-desktop-bar-git)

```txt
ssh://aur@aur.archlinux.org/plasma5-applets-virtual-desktop-bar-git.git
```

## packaging

```sh
#Generate .SRCINFO
makepkg --printsrcinfo > .SRCINFO
```

## ssh help

```sh
# list keys
ssh-add -l

# adding existing key
ssh-add ~/.ssh/aur

# create new
ssh-keygen -f ~/.ssh/aur
```

Permissions 0644 for 'aur.pub' are too open.

```sh
chmod 400 ~/.ssh/aur
```

Associate key with url

```sh
# add next to ~/.ssh/config:
Host aur.archlinux.org
  IdentityFile ~/.ssh/aur
  User aur
```
